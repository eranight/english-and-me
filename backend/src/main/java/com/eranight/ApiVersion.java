package com.eranight;

public final class ApiVersion {

    public static final String VERSION = "/api/v1";

    public static String endpoint(String suffix) {
        return VERSION + (suffix.startsWith("/") ? suffix : "/" + suffix);
    }

}
