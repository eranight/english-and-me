package com.eranight.authentication;

import com.eranight.ApiVersion;
import com.eranight.authentication.model.*;
import com.eranight.exception.BusinessValidationException;
import com.eranight.user.User;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
@RequestMapping(ApiVersion.VERSION + "/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/login")
    public ResponseEntity<LoginResponseForm> login(@RequestBody LoginForm form) {
        LoginResponseForm loginResponseForm = authenticationService.login(form);
        return ResponseEntity.ok(loginResponseForm);
    }

    @PostMapping("/register")
    public ResponseEntity<Void> register(@RequestBody RegistrationForm form) {
        if (!Objects.equals(form.password(), form.confirmedPassword())) {
            throw new BusinessValidationException("confirmedPassword", "must match password");
        }
        authenticationService.register(form);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/logout")
    public ResponseEntity<String> logout(@AuthenticationPrincipal User user) {
        authenticationService.logout(user);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/refresh-token")
    public ResponseEntity<RefreshTokenResponse> refreshToken(@RequestBody RefreshTokenRequest request, @AuthenticationPrincipal User user) {
        RefreshTokenResponse refreshTokenResponse = authenticationService.refreshToken(user, request);
        return ResponseEntity.ok(refreshTokenResponse);
    }

}
