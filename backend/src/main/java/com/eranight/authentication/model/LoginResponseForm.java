package com.eranight.authentication.model;

import org.apache.commons.lang3.StringUtils;

public record LoginResponseForm(String token, String refreshToken) {
    public LoginResponseForm {
        if (StringUtils.isBlank(token)) {
            throw new IllegalArgumentException("token must not be empty or null");
        }
        if (StringUtils.isBlank(refreshToken)) {
            throw new IllegalArgumentException("refresh token must not be empty or null");
        }
    }
}
