package com.eranight.authentication.model;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

public record RegistrationForm(String username, String password, String confirmedPassword) {

    public RegistrationForm {
        if (StringUtils.isBlank(username)) {
            throw new IllegalArgumentException("username must not be empty or null");
        }
        if (StringUtils.isBlank(password)) {
            throw new IllegalArgumentException("password must not be empty or null");
        }
        if (!Objects.equals(password, confirmedPassword)) {
            throw new IllegalArgumentException("confirmedPassword must match password");
        }
    }

}
