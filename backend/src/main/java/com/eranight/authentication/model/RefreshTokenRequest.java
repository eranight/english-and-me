package com.eranight.authentication.model;

public record RefreshTokenRequest(String refreshToken) {
}
