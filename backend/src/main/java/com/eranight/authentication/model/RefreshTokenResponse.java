package com.eranight.authentication.model;

public record RefreshTokenResponse(String token) {
}
