package com.eranight.authentication.model;

import org.apache.commons.lang3.StringUtils;

public record LoginForm(String username, String password) {

    public LoginForm {
        if (StringUtils.isBlank(username)) {
            throw new IllegalArgumentException("username must not be empty or null");
        }
        if (StringUtils.isBlank(password)) {
            throw new IllegalArgumentException("password must not be empty or null");
        }
    }

}
