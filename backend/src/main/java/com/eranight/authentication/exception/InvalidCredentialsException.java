package com.eranight.authentication.exception;

import com.eranight.exception.BusinessLogicException;
import org.springframework.http.HttpStatus;

public class InvalidCredentialsException extends BusinessLogicException {

    private static final String TYPE = "INVALID_CREDENTIALS";

    public InvalidCredentialsException() {
        super(TYPE, "invalid credentials", HttpStatus.UNAUTHORIZED);
    }

}
