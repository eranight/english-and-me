package com.eranight.authentication;

import com.eranight.authentication.exception.InvalidCredentialsException;
import com.eranight.authentication.model.*;
import com.eranight.security.jwt.JwtTokenService;
import com.eranight.security.refreshtoken.RefreshToken;
import com.eranight.security.refreshtoken.RefreshTokenService;
import com.eranight.user.AuthorityEnum;
import com.eranight.user.User;
import com.eranight.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenService jwtTokenService;
    private final RefreshTokenService refreshTokenService;

    public void register(RegistrationForm form) {
        userService.create(form.username(), passwordEncoder.encode(form.password()), Collections.singleton(AuthorityEnum.USER));
    }

    public LoginResponseForm login(LoginForm form) {
        User user = userService.findUser(form.username());
        if (!passwordEncoder.matches(form.password(), user.getPassword())) {
            throw new InvalidCredentialsException();
        }

        String jwtToken = jwtTokenService.issue(user).getTokenValue();
        RefreshToken refreshToken = refreshTokenService.issue(user);

        return new LoginResponseForm(jwtToken, refreshToken.value().toString());
    }

    @Transactional
    public void logout(User user) {
        refreshTokenService.removeAllFor(user);
    }

    public RefreshTokenResponse refreshToken(User user, RefreshTokenRequest request) {
        refreshTokenService.verify(new RefreshToken(UUID.fromString(request.refreshToken())));
        String jwtToken = jwtTokenService.issue(user).getTokenValue();
        return new RefreshTokenResponse(jwtToken);
    }

}
