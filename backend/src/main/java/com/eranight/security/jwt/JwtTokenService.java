package com.eranight.security.jwt;

import com.eranight.user.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;

@Service
public class JwtTokenService {

    private final String issuer;
    private final Duration expirationTime;
    private final JwtEncoder jwtEncoder;

    public JwtTokenService(@Value("${application.jwt.token.issuer}") String issuer,
                           @Value("${application.jwt.token.expiration-time}") Duration expirationTime,
                           JwtEncoder jwtEncoder) {
        this.issuer = issuer;
        this.expirationTime = expirationTime;
        this.jwtEncoder = jwtEncoder;
    }

    public Jwt issue(User user) {
        var now = Instant.now();
        JwtClaimsSet claimsSet = JwtClaimsSet.builder()
                .issuer(issuer)
                .issuedAt(now)
                .expiresAt(now.plus(expirationTime))
                .claim("principal", user.getUserId())
                .build();
        return jwtEncoder.encode(JwtEncoderParameters.from(claimsSet));
    }

}
