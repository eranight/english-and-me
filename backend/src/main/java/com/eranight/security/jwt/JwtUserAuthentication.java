package com.eranight.security.jwt;

import com.eranight.user.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

import java.util.Collection;
import java.util.stream.Collectors;

public class JwtUserAuthentication extends JwtAuthenticationToken {

    private final User user;

    public JwtUserAuthentication(Jwt jwt, User user) {
        super(jwt);
        this.user = user;
    }

    @Override
    public Object getPrincipal() {
        return user;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return user.getAuthorities().stream().map(ath -> (GrantedAuthority) ath).collect(Collectors.toSet());
    }
}
