package com.eranight.security.jwt;

import com.eranight.user.mapper.UserMapper;
import com.eranight.user.persistence.UserEntity;
import com.eranight.user.persistence.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationConverter implements Converter<Jwt, AbstractAuthenticationToken> {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Override
    public AbstractAuthenticationToken convert(Jwt jwt) {
        UUID userId = UUID.fromString(jwt.getClaim("principal"));
        Optional<UserEntity> userOptional = userRepository.findByUserId(userId);
        if (userOptional.isEmpty()) {
            return null;
        }

        var user = userMapper.toDomain(userOptional.get());
        return new JwtUserAuthentication(jwt, user);
    }
}
