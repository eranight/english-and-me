package com.eranight.security.jwt;

import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtEncoder;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

@Configuration
public class JwtConfiguration {

    @Bean
    public JwtEncoder jwtEncoder(RSAPublicKey publicKey, RSAPrivateKey privateKey) {
        RSAKey rsaKey = new RSAKey.Builder(publicKey).privateKey(privateKey).build();
        return new NimbusJwtEncoder((jwkSelector, context) -> jwkSelector.select(new JWKSet(rsaKey)));
    }

    @Bean
    public JwtDecoder jwtDecoder(RSAPublicKey publicKey) {
        return NimbusJwtDecoder.withPublicKey(publicKey).build();
    }

    @Bean
    public RSAPublicKey publicKey(@Qualifier("jwtKeyStore") KeyStore keyStore, @Value("${application.jwt.keystore.alias}") String alias) throws KeyStoreException {
        return (RSAPublicKey) keyStore.getCertificate(alias).getPublicKey();
    }

    @Bean
    public RSAPrivateKey privateKey(@Qualifier("jwtKeyStore") KeyStore keyStore, @Value("${application.jwt.keystore.alias}") String alias, @Value("${application.jwt.keystore.password}") String password) throws UnrecoverableEntryException, NoSuchAlgorithmException, KeyStoreException {
        return (RSAPrivateKey) ((KeyStore.PrivateKeyEntry) keyStore.getEntry(alias, new KeyStore.PasswordProtection(password.toCharArray()))).getPrivateKey();
    }

    @Bean
    public KeyStore jwtKeyStore(@Value("${application.jwt.keystore.path}") String path,
                                @Value("${application.jwt.keystore.password}") String password,
                                @Qualifier("webApplicationContext") ResourceLoader resourceLoader) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(resourceLoader.getResource(path).getInputStream(), password.toCharArray());

        return keyStore;
    }

}
