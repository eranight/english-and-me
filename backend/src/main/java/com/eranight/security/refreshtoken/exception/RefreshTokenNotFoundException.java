package com.eranight.security.refreshtoken.exception;

import com.eranight.exception.BusinessLogicException;
import org.springframework.http.HttpStatus;

public class RefreshTokenNotFoundException extends BusinessLogicException {

    public static final String TYPE = "REFRESH_TOKEN_NOT_FOUND";

    public RefreshTokenNotFoundException(String token) {
        super(TYPE, toDetail(token), HttpStatus.NOT_FOUND);
    }

    private static String toDetail(String token) {
        return "Refresh token [%s] not found".formatted(token);
    }

}
