package com.eranight.security.refreshtoken.persistence;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.UUID;

@Entity
@Table(schema = "user_info", name = "refresh_token")
@Getter
@Setter
public class RefreshTokenEntity {
    @Id
    private UUID id;
    private Instant issueDate;
    private Instant expirationDate;
    private int userId;
}
