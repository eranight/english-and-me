package com.eranight.security.refreshtoken;

import java.util.UUID;

public record RefreshToken(UUID value) {
}
