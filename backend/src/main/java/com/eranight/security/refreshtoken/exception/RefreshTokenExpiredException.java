package com.eranight.security.refreshtoken.exception;

import com.eranight.exception.BusinessLogicException;
import org.springframework.http.HttpStatus;

public class RefreshTokenExpiredException extends BusinessLogicException {

    public static final String TYPE = "REFRESH_TOKEN_EXPIRED";

    public RefreshTokenExpiredException(String token) {
        super(TYPE, toDetail(token), HttpStatus.UNAUTHORIZED);
    }

    private static String toDetail(String token) {
        return "Refresh token [%s] expired".formatted(token);
    }

}
