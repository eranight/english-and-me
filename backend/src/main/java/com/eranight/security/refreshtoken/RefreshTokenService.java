package com.eranight.security.refreshtoken;

import com.eranight.security.refreshtoken.exception.RefreshTokenExpiredException;
import com.eranight.security.refreshtoken.exception.RefreshTokenNotFoundException;
import com.eranight.security.refreshtoken.persistence.RefreshTokenEntity;
import com.eranight.security.refreshtoken.persistence.RefreshTokenRepository;
import com.eranight.user.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

@Service
public class RefreshTokenService {

    private final Duration expirationTime;
    private final RefreshTokenRepository repository;

    public RefreshTokenService(@Value("${application.refresh-token.expiration-time}") Duration expirationTime,
                               RefreshTokenRepository repository
    ) {
        this.expirationTime = expirationTime;
        this.repository = repository;
    }

    public RefreshToken issue(User user) {
        UUID token = UUID.randomUUID();
        Instant issueDate = Instant.now();
        Instant expirationDate = issueDate.plus(expirationTime);

        RefreshTokenEntity tokenEntity = new RefreshTokenEntity();
        tokenEntity.setId(token);
        tokenEntity.setIssueDate(issueDate);
        tokenEntity.setExpirationDate(expirationDate);
        tokenEntity.setUserId(user.getId());

        repository.saveAndFlush(tokenEntity);

        return new RefreshToken(token);
    }

    public void verify(RefreshToken refreshToken) {
        RefreshTokenEntity tokenEntity = repository
                .findById(refreshToken.value())
                .orElseThrow(() -> new RefreshTokenNotFoundException(refreshToken.value().toString()));
        if (tokenEntity.getExpirationDate().isBefore(Instant.now())) {
            repository.delete(tokenEntity);
            throw new RefreshTokenExpiredException(refreshToken.value().toString());
        }
    }

    public void removeAllFor(User user) {
        repository.deleteAllByUserId(user.getId());
    }

}
