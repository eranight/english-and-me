package com.eranight.user;

import lombok.Getter;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Set;
import java.util.UUID;

@Getter
public class User implements UserDetails {

    private final int id;
    private final UUID userId;
    private final String username;
    private final String password;
    private final Set<AuthorityEnum> authorities;

    public User(int id, UUID userId, String username, String password, Set<AuthorityEnum> authorities) {
        this.id = id;
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
