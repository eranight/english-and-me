package com.eranight.user.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<AuthorityEntity, Integer> {

    Optional<AuthorityEntity> findByCode(String code);

    List<AuthorityEntity> findAllByCodeIsIn(Collection<String> codes);

}
