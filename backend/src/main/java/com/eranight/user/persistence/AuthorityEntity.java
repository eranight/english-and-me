package com.eranight.user.persistence;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(schema = "user_info", name = "authority")
public class AuthorityEntity {
    @Id
    public int id;
    public String code;
}
