package com.eranight.user;

import org.springframework.security.core.GrantedAuthority;

public enum AuthorityEnum implements GrantedAuthority {
    ADMIN("admin"),
    USER("user");

    public final String value;

    AuthorityEnum(String value) {
        this.value = value;
    }

    @Override
    public String getAuthority() {
        return "ROLE_" + name();
    }

    public static AuthorityEnum fromValue(String value) {
        for (AuthorityEnum authority : AuthorityEnum.values()) {
            if (authority.value.equals(value)) {
                return authority;
            }
        }
        return null;
    }
}
