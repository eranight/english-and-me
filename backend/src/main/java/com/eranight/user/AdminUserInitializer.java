package com.eranight.user;

import com.eranight.user.persistence.AuthorityEntity;
import com.eranight.user.persistence.AuthorityRepository;
import com.eranight.user.persistence.UserEntity;
import com.eranight.user.persistence.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
@RequiredArgsConstructor
public class AdminUserInitializer {

    private final PasswordEncoder passwordEncoder;
    private final AuthorityRepository authorityRepository;
    private final UserRepository userRepository;

    @EventListener(ApplicationReadyEvent.class)
    public void createAdminIfNotExist() {
        if (userRepository.existsByUsername("admin")) {
            return;
        }

        UserEntity entity = new UserEntity();
        entity.setUsername("admin");
        entity.setPassword(passwordEncoder.encode("password"));
        AuthorityEntity adminAuthority = authorityRepository
                .findByCode(AuthorityEnum.ADMIN.value)
                .orElseThrow(() -> new RuntimeException("admin authority not found"));
        entity.setAuthorities(Collections.singletonList(adminAuthority));

        userRepository.saveAndFlush(entity);
    }

}
