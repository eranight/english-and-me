package com.eranight.user;

import com.eranight.user.exception.UsernameAlreadyExistsException;
import com.eranight.user.exception.UsernameNotExistException;
import com.eranight.user.mapper.UserMapper;
import com.eranight.user.persistence.AuthorityEntity;
import com.eranight.user.persistence.AuthorityRepository;
import com.eranight.user.persistence.UserEntity;
import com.eranight.user.persistence.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository repository;
    private final AuthorityRepository authorityRepository;
    private final UserMapper mapper;

    public User create(String username, String password, Set<AuthorityEnum> authorities) {
        if (repository.existsByUsername(username)) {
            throw new UsernameAlreadyExistsException(username);
        }

        UserEntity entity = new UserEntity();
        entity.setUsername(username);
        entity.setPassword(password);

        List<AuthorityEntity> codes = authorityRepository.findAllByCodeIsIn(authorities.stream().map(it -> it.value).toList());
        entity.setAuthorities(codes);

        return mapper.toDomain(repository.saveAndFlush(entity));
    }

    public User findUser(String username) {
        return repository
                .findByUsername(username)
                .map(mapper::toDomain)
                .orElseThrow(() -> new UsernameNotExistException(username));
    }

}
