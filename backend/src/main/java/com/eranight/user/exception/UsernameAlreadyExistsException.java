package com.eranight.user.exception;

import com.eranight.exception.BusinessLogicException;
import org.springframework.http.HttpStatus;

public class UsernameAlreadyExistsException extends BusinessLogicException {

    private static final String TYPE = "ALREADY_EXISTS_WITH_USERNAME";

    public UsernameAlreadyExistsException(String username) {
        super(TYPE, toDetail(username), HttpStatus.BAD_REQUEST);
    }

    private static String toDetail(String username) {
        return "username \\'%s\\' already exists".formatted(username);
    }

}
