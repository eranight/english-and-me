package com.eranight.user.exception;

import com.eranight.exception.BusinessLogicException;
import org.springframework.http.HttpStatus;

public class UsernameNotExistException extends BusinessLogicException {

    private static final String TYPE = "USERNAME_NOT_FOUND";

    public UsernameNotExistException(String username) {
        super(TYPE, toDetail(username), HttpStatus.BAD_REQUEST);
    }

    private static String toDetail(String username) {
        return "username \\'%s\\' does not exist".formatted(username);
    }

}
