package com.eranight.user.mapper;

import com.eranight.user.AuthorityEnum;
import com.eranight.user.User;
import com.eranight.user.persistence.AuthorityEntity;
import com.eranight.user.persistence.UserEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User toDomain(UserEntity entity);

    default AuthorityEnum toDomain(AuthorityEntity entity) {
        return AuthorityEnum.fromValue(entity.code);
    }

}
