package com.eranight.exception;

import java.util.List;

public record ErrorView(String error, List<String> reasons) {
}
