package com.eranight.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.Objects;

@Getter
public class BusinessLogicException extends BaseException {
    private final String type;
    private final String detail;
    private final HttpStatus httpStatus;

    public BusinessLogicException(String type, String detail, HttpStatus httpStatus) {
        this.type = Objects.requireNonNull(type);
        this.detail = detail;
        this.httpStatus = Objects.requireNonNull(httpStatus);
    }
}
