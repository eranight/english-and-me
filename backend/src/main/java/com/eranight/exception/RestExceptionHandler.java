package com.eranight.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.support.WebExchangeBindException;

import java.util.Collections;
import java.util.List;

@RestControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(WebExchangeBindException.class)
    ResponseEntity<ErrorView> validationError(WebExchangeBindException e) {
        List<String> errors = e.getFieldErrors()
                .stream()
                .map(fieldError -> String.join(": ", fieldError.getField(), fieldError.getDefaultMessage()))
                .toList();
        return ResponseEntity.badRequest().body(new ErrorView("VALIDATION_ERROR", errors));
    }

    @ExceptionHandler(BusinessLogicException.class)
    ResponseEntity<ErrorView> businessLogicException(BusinessLogicException e) {
        return new ResponseEntity<>(new ErrorView(e.getType(), List.of(e.getDetail())), e.getHttpStatus());
    }

    @ExceptionHandler(BaseSystemException.class)
    ResponseEntity<ErrorView> systemException(BaseSystemException e) {
        return new ResponseEntity<>(new ErrorView(e.getType(), List.of(e.getDetail())), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(OAuth2AuthenticationException.class)
    ResponseEntity<ErrorView> oauthException(OAuth2AuthenticationException e) {
        return new ResponseEntity<>(new ErrorView(e.getError().getErrorCode(), Collections.emptyList()), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(AuthenticationException.class)
    ResponseEntity<ErrorView> authentication(AuthenticationException e) {
        return new ResponseEntity<>(new ErrorView(e.getMessage(), Collections.emptyList()), HttpStatus.UNAUTHORIZED);
    }

}
