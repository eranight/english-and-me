package com.eranight.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public abstract class BaseSystemException extends BaseException {
    private final String type;
    private final String detail;
}
