package com.eranight.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class BusinessValidationException extends BusinessLogicException {

    private static final String TYPE = "VALIDATION_ERROR";

    public BusinessValidationException(String fieldName, String constraint) {
        super(TYPE, toDetail(fieldName, constraint), HttpStatus.BAD_REQUEST);
    }

    private static String toDetail(String fieldName, String constraint) {
        return "%s: %s".formatted(fieldName, constraint);
    }
}
