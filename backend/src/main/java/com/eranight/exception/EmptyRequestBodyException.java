package com.eranight.exception;

import org.springframework.http.HttpStatus;

public class EmptyRequestBodyException extends BusinessLogicException {

    public static final String TYPE = "EMPTY_REQUEST_BODY";

    public EmptyRequestBodyException() {
        super(TYPE, "the method does not support empty body", HttpStatus.BAD_REQUEST);
    }
}
