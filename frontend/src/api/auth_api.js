import axios from "axios";
import { api } from "@/api/api_version.js";

export const AuthApi = {

  async Register(body) {
    await axios.post(api('auth/register'), body)
  },

  async Login(body) {
    return await axios.post(api('auth/login'), body)
  },

  async Logout() {
    await axios.post(api('auth/logout'))
  },

  async RefreshToken(body) {
    return await axios.post(api('auth/refresh-token'), body)
  }

}