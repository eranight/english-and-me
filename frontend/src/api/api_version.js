export const ApiVersion = "/api/v1"

export function api(suffix) {
  return `${ApiVersion}${suffix.startsWith('/') ? suffix : '/' + suffix}`
}