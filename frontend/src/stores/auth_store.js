import { defineStore } from 'pinia';
import { ref } from "vue";

export const auth_store = defineStore('auth_store', () => {

  const bearerToken = ref(null)
  const refreshToken = ref(null)

  function setBearerToken(token) {
    bearerToken.value = token
  }

  function setRefreshToken(token) {
    refreshToken.value = token
  }

  return { bearerToken, refreshToken, setBearerToken, setRefreshToken }

}, { persist: true } )