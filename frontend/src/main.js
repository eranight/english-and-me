import { createApp } from 'vue'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import '@/assets/css/main.css'
import '@/assets/css/shake.css'
import '@fortawesome/fontawesome-free/css/all.css'
import App from '@/App.vue'
import router from "@/router"
import axios from 'axios'

const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)

axios.defaults.baseURL = 'http://localhost:8080'
axios.defaults.withCredentials = true

createApp(App)
  .use(pinia)
  .use(router)
  .mount('#app')
