import com.github.gradle.node.npm.task.NpmTask


plugins {
    id("com.github.node-gradle.node") version "7.0.2"
}

node {
    download = true
    version = "18.5.0"
}

tasks.register<NpmTask>("npmBuild") {
    dependsOn("npmInstall")
    args.addAll("run", "build")
}

task("build") {
    dependsOn("npmBuild")
}